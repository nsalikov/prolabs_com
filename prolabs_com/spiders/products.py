# -*- coding: utf-8 -*-
import json
import scrapy
from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode


class ProductsSpider(scrapy.Spider):
    name = 'products'
    allowed_domains = ['prolabs.com']
    start_urls = [
        # Transceivers
        'https://www.prolabs.com/api/search?term=&categorySlug=transceivers&page=1&perPage=15&categoriesIds%5B%5D=3',
        # Direct Attach Cables / Active Optical Cables
        'https://www.prolabs.com/api/search?term=&categorySlug=dacs-aoc&page=1&perPage=15&categoriesIds%5B%5D=4',
        # Active Solutions - Media Converters
        'https://www.prolabs.com/api/search?term=&categorySlug=media-converters&page=1&perPage=15&categoriesIds%5B%5D=10',
        # Active Solutions - OTDR and Network Services Validation
        'https://www.prolabs.com/api/search?term=&categorySlug=otdr&page=1&perPage=15&categoriesIds%5B%5D=38',
        # Active Solutions - EON-OMP-2 MultiService Platform
        'https://www.prolabs.com/api/search?term=&categorySlug=eon-omp-2&page=1&perPage=15&categoriesIds%5B%5D=52',
        # Active Solutions - OCM Optical Channel Devices
        'https://www.prolabs.com/api/search?term=&categorySlug=eon-omp-ocm&page=1&perPage=15&categoriesIds%5B%5D=43',
        # Passive Solutions
        'https://www.prolabs.com/api/search?term=&categorySlug=passive-solutions&page=1&perPage=15&categoriesIds%5B%5D=5&categoriesIds%5B%5D=11',
        # Memory Upgrades
        'https://www.prolabs.com/api/search?term=&categorySlug=memory-upgrades&page=1&perPage=15&categoriesIds%5B%5D=14&categoriesIds%5B%5D=15&categoriesIds%5B%5D=16',
        # Accessory Cables
        'https://www.prolabs.com/api/search?term=&categorySlug=accessories&page=1&perPage=15&categoriesIds%5B%5D=24&categoriesIds%5B%5D=27&categoriesIds%5B%5D=29',
    ]


    def parse(self, response):
        try:
            data = json.loads(response.text)
        except Exception as e:
            self.logger.warning('Unable to parse json', exc_info=True)
            self.logger.debug(response.text)
            return

        for system in data['systems']:
            for item in system['products']:
                yield response.follow(item['url'], meta={'item': item}, callback=self.parse_item)

        currentPage = int(data['meta']['currentPage'])
        lastPage = int(data['meta']['lastPage'])

        if currentPage < lastPage:
            next_url = self.get_next_page(response)
            yield response.follow(next_url, callback=self.parse)


    def parse_item(self, response):
        d = {}

        d['url'] = response.url

        try:
            json_data = response.css('script[type="application/ld+json"] ::text').extract_first()
            data = json.loads(json_data)
            d['breadcrumbs'] = [e['name'] for e in data['@graph'][3]['itemListElement'][2:]]
        except Exception as e:
            self.logger.warning('Unable to parse json', exc_info=True)
            self.logger.debug(json_data)
            d['breadcrumbs'] = []

        d['sku'] = response.css('header h1::text').extract_first()
        d['name'] = response.css('header h2::text').extract_first()
        d['description'] = ' '.join(filter(None, [t.strip() for t in response.css('header>p *::text').extract()]))
        d['specs'] = parse_specs(response)

        return d


    def get_next_page(self, response):
        request_url = response.request.url
        next_url = None

        p = list(urlparse(request_url))
        q = dict(parse_qsl(p[4]))

        try:
            q['page'] = int(q['page']) + 1
            p[4] = urlencode(q)
            next_url = urlunparse(p)
        except:
            self.logger.warning('Unable to parse url. Something may be wrong with <{}>'.format(request_url))

        return next_url


def parse_specs(response):
    d = {}

    for table in response.css('#product-spec table'):
        table_name = table.css('table th[colspan="2"] ::text').extract_first()

        if table_name:
            table_name = table_name.strip()
        else:
            table_name = 'NA'

        if table_name not in d:
            d[table_name] = {}

        for tr in table.css('table tr'):
            key = ' '.join(filter(None, [t.strip() for t in tr.css('tr>td:nth-child(1) ::text').extract()]))
            value = ' '.join(filter(None, [t.strip() for t in tr.css('tr>td:nth-child(2) ::text').extract()]))

            if key and value:
                d[table_name][key] = value

    return d
